import numpy as np
import pandas as pd
from itertools import groupby

def GetDistance(data, row, col):
  return data.iloc[row-1][col-1]

def Init():
    return pd.read_csv("datajarak.csv")

def InitialPopulation(nPopulasi, nGen):
  populasi = []
  while (len(populasi) < nPopulasi):
      firefly=[1, 2, 3]
      firefly.extend(np.random.choice(range(4,nGen+1), nGen-3, replace=False))
      populasi.append(firefly)
      EvaluatePopulasi(populasi)
  return populasi

def EvaluatePopulasi(populasi):
  return [k for k,v in groupby(sorted(populasi))]
  
def SwapWithRouteDetector(data, selectedFirefly):
  i = 0
  mutant = [selectedFirefly[i]]
  while (i < len(selectedFirefly)-1) and (GetDistance(data, selectedFirefly[i], selectedFirefly[i+1]) != 0):
    i+=1
    mutant.append(selectedFirefly[i])
  j = i+1
  while (j < len(selectedFirefly)-1) and (GetDistance(data, selectedFirefly[i], selectedFirefly[j]) == 0):
    j+=1
  mutant.append(selectedFirefly[j])
  k = j+1
  while (k < len(selectedFirefly)-1) and (GetDistance(data, selectedFirefly[k], selectedFirefly[k+1]) != 0):
    k+=1
    mutant.append(selectedFirefly[k])
  for l in range(i+1, j):
    mutant.append(selectedFirefly[l])
  for m in range(k, len(selectedFirefly)):
    mutant.append(selectedFirefly[m])
  return mutant

def TestSwapWithRouteDetector(data):
  nPopulasi = 20
  nGen = 18
  populasi = InitialPopulation(nPopulasi, nGen)
  for i in range(len(populasi)):
    print('Before: ', populasi[i])
    populasi[i] = SwapWithRouteDetector(data, populasi[i])
    print('After: ', populasi[i])

def Main():
    np.seterr(divide='ignore', invalid='ignore')
    data = Init()
    TestSwapWithRouteDetector(data)

if __name__ == "__main__":
    Main()
