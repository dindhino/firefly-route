import numpy as np
import pandas as pd
from numpy import inf
import math
from itertools import groupby

def CalculateTime(jarak, kecepatan):
    return 0 if kecepatan == 0 else jarak/kecepatan

def GetDistance(data, row, col):
    return data.iloc[row-1][col-1]

def CalculateDistance(data, rute):
    totalDistance = 0
    for i in range(len(rute)-1):
        distance = GetDistance(data, rute[i], rute[i+1])
        if (distance == 0):
            return inf #No edge found
        totalDistance += distance
        if (rute[i+1] == 9):
            return totalDistance #End Route
    return totalDistance

def PrintStatus(statusRoute, rute, time):
    print(statusRoute, rute, '\nTime: ', time, '\n\n')

def Init():
    data = pd.read_csv("datajarak.csv")
    rute_utama = []
    rute_utama.extend(range(1,10))
    jarak_rute_utama = CalculateDistance(data, rute_utama)
    kecepatan_rute_utama = 83.3333 # m/menit = 5km/jam
    PrintStatus('Main Route: ', rute_utama, CalculateTime(jarak_rute_utama, kecepatan_rute_utama))
    return data

def CalculateLightIntensity(data, kecepatan, rute):
    jarak = CalculateDistance(data, rute)
    if (jarak == inf):
        return 0
    time = CalculateTime(jarak, kecepatan)
    return 0 if time == 0 else 1/time

def GetLightIntensityFromAllFireflies(populasi, data, kecepatan):
    return [CalculateLightIntensity(data, kecepatan, rute) for rute in populasi]

def InitialPopulation(nPopulasi, nGen):
    populasi = []
    populasi.append([i for i in range(1, nGen+1)]) #Cheating: Latin Hypercube Sampling
    for i in range(nPopulasi):
        firefly=[1]
        firefly.extend(np.random.choice(range(2,nGen+1), nGen-1, replace=False)) #Random permutasi, without replacement
        populasi.append(firefly)
    return populasi

def OrderPopulation(light, populasi):
    indeks = np.argsort(light, axis=0) #sorting ascending
    light_ = [light[idx] for idx in indeks]
    populasi_ = [populasi[idx] for idx in indeks]
    return light_, populasi_

def EvaluatePopulasi(populasi):
  return [k for k,v in groupby(sorted(populasi))]

def FireflyAlgorithm(data):
    kecepatan_rute_alt = 333.333  # m/menit = 20km/jam
    gamma = 0.01; # Absorption coefficient
    beta0 = 1 # Attractiveness constant
    alpha=1.0; # Randomness strength 0--1 (highly random)
    theta=0.97; # Randomness reduction factor theta=10^(-5/tMax)
    nPopulasi = 20
    nGen = 18 # Dimension
    lb=2;
    ub=18;
    scale = ub - lb
    bestFirefly = []
    solutionFound = False
    populasi = InitialPopulation(nPopulasi, nGen)
    tempPopulasi = []
    light = GetLightIntensityFromAllFireflies(populasi, data, kecepatan_rute_alt)
    light_, populasi_ = OrderPopulation(light, populasi)
    if (light_[len(light_)-1] > 0):
        solutionFound = True
        bestFirefly = populasi_[len(populasi_)-1]
    while (solutionFound == False):
        alpha *= theta
        populasi.extend(tempPopulasi)
        populasi = EvaluatePopulasi(populasi)
        light = GetLightIntensityFromAllFireflies(populasi, data, kecepatan_rute_alt)
        light_, populasi_ = OrderPopulation(light, populasi)
        if (light_[len(light_)-1] > 0):
            solutionFound = True
            bestFirefly = populasi_[len(populasi_)-1]
            break
        tempPopulasi = []
        for i in range(len(populasi_)):
            for j in range(i):
                if light_[j] >= light_[i]:
                    selectedFirefly = populasi_[i]
                    r=np.sqrt(np.sum([k**2 for k in ([a-b for a,b in zip(populasi_[i], populasi_[j])])])) #Euclidean distance
                    beta = beta0*math.exp(-gamma*(r**2))
                    indexForSwap = int(scale * np.random.uniform(0,1,1) + lb)
                    if alpha*np.random.uniform(0,1,1) > 0.5:
                        index2ForSwap = indexForSwap + int(beta) + lb
                    else:
                        index2ForSwap = indexForSwap - int(beta) - lb
                    if (index2ForSwap > ub):
                        selectedFirefly[indexForSwap-1], selectedFirefly[ub-1] =  selectedFirefly[ub-1], selectedFirefly[indexForSwap-1]
                    elif (index2ForSwap < lb):
                        selectedFirefly[indexForSwap-1], selectedFirefly[lb-1] =  selectedFirefly[lb-1], selectedFirefly[indexForSwap-1]
                    else:
                        selectedFirefly[indexForSwap-1], selectedFirefly[index2ForSwap-1] =  selectedFirefly[index2ForSwap-1], selectedFirefly[indexForSwap-1]
                    tempPopulasi.append(selectedFirefly)
    print('Best Firefly: ', bestFirefly)
    alternate_route = bestFirefly[0:bestFirefly.index(9)+1]
    PrintStatus('Altenative Route: ', alternate_route, CalculateTime(CalculateDistance(data, alternate_route), kecepatan_rute_alt))

def Main():
    np.seterr(divide='ignore', invalid='ignore')
    data = Init()
    FireflyAlgorithm(data)

if __name__ == "__main__":
    Main()
